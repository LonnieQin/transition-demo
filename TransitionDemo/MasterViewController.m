//
//  MasterViewController.m
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Detail2ViewController.h"
@interface MasterViewController ()
{
    NSMutableArray * _objects;
}
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
}

#pragma mark - Table view data source
- (void) insertNewObject:(id) sender
{
    if (!_objects) {
        _objects = [NSMutableArray new];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath * indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDate * date = _objects[indexPath.row];
    cell.textLabel.text = date.description;
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.transionType == TransitionTypeDetail1) {
        DetailViewController * detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detail1"];
        detailViewController.detailItem = _objects[indexPath.row];
        
        detailViewController.transitioningDelegate = self;
        [self.navigationController presentViewController:detailViewController animated:YES completion:nil];
    } else {
        Detail2ViewController * detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"detail2"];
        detailViewController.detailItem = _objects[indexPath.row];
        
        detailViewController.transitioningDelegate = self;
        [self.navigationController presentViewController:detailViewController animated:YES completion:nil];
    }

}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - <UIViewControllerTransitionDelegate>
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return self;
}

/*
- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id <UIViewControllerAnimatedTransitioning>)animator
{
    return self;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator
{
    return self;
    
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source
{
    return self;
}
*/

#pragma mark - <UIViewControllerAnimatedTransitioning>

// This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
// synchronize with the main animation.
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 1;
}
// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController * src = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController * dest = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    CGRect f = src.view.frame;
    CGRect originalSourceRect = src.view.frame;
    f.origin.y = f.size.height;
    
    [UIView animateWithDuration:0.5 animations:^{
        src.view.frame = f;
    } completion:^(BOOL finished) {
        src.view.alpha = 0;
        dest.view.frame = f;
        dest.view.alpha = 0.0f;
        [[src.view superview] addSubview:dest.view];
        [UIView animateWithDuration:0.5 animations:^{
            dest.view.frame = originalSourceRect;
            dest.view.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [src.view removeFromSuperview];
            src.view.alpha = 1.0f;
            [transitionContext completeTransition:YES];
        }];
    }];
}

@end
