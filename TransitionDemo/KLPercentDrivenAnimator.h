//
//  KLPercentDrivenAnimator.h
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KLPercentDrivenAnimator : UIPercentDrivenInteractiveTransition
- (instancetype) initWithViewController:(UIViewController*)controller;
- (void) pinchGestureAction:(UIPinchGestureRecognizer*) gestureRecognizer;
@end
