//
//  DetailViewController.m
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController
- (void) setDetailItem:(id)detailItem
{
    _detailItem = detailItem;
    [self configureView];
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%s",__func__);
    [self configureView];
}
- (void) configureView
{
    self.detailLabel.text = [self.detailItem description];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
