//
//  MasterViewController.h
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    TransitionTypeDetail1,
    TransitionTypeDetail2
}TransitionType;
@interface MasterViewController : UITableViewController<UIViewControllerTransitioningDelegate,UIViewControllerAnimatedTransitioning>
@property (nonatomic,assign) TransitionType transionType;
@end
