//
//  KLPercentDrivenAnimator.m
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "KLPercentDrivenAnimator.h"
@interface KLPercentDrivenAnimator()
@property (nonatomic,assign) CGFloat startScale;
@property (weak,nonatomic) UIViewController * controller;
@end
@implementation KLPercentDrivenAnimator
- (instancetype) initWithViewController:(UIViewController*)controller
{
    self = [super init];
    if (self) {
        self.controller = controller;
    }
    return self;
}
- (void) pinchGestureAction:(UIPinchGestureRecognizer*) gestureRecognizer
{
    CGFloat scale = gestureRecognizer.scale;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.startScale = scale;
        [self.controller dismissViewControllerAnimated:YES completion:nil];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat completePercent = 1.0 - (scale/self.startScale);
        [self updateInteractiveTransition:completePercent];
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (gestureRecognizer.velocity >= 0) {
            [self cancelInteractiveTransition];
        } else {
            [self finishInteractiveTransition];
        }
    } else if (gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        [self cancelInteractiveTransition];
    }
}
@end
