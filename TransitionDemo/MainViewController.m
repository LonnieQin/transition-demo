//
//  MainViewController.m
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MainViewController.h"
#import "MasterViewController.h"
@interface MainViewController ()

@end

@implementation MainViewController
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ((UIViewController*)segue.destinationViewController).title  =  ((UITableViewCell*)sender).textLabel.text;
    if ([segue.identifier isEqualToString:@"master1"]) {
        MasterViewController * controller = segue.destinationViewController;
        controller.transionType= TransitionTypeDetail1;
    }
    if ([segue.identifier isEqualToString:@"master2"]) {
        MasterViewController * controller = segue.destinationViewController;
        controller.transionType= TransitionTypeDetail2;
    }
}
@end
