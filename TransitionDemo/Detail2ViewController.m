//
//  Detail2ViewController.m
//  TransitionDemo
//
//  Created by amttgroup on 15-5-27.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "Detail2ViewController.h"
#import "KLPercentDrivenAnimator.h"
@interface Detail2ViewController ()<UIViewControllerTransitioningDelegate,UIViewControllerAnimatedTransitioning,UIViewControllerInteractiveTransitioning>
@property (strong,nonatomic) KLPercentDrivenAnimator * animator;
@end

@implementation Detail2ViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        [self configureView];
    }
}


#pragma  mark - View Life Cycle
- (void) viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
    self.animator = [[KLPercentDrivenAnimator alloc] initWithViewController:self];
    UIPinchGestureRecognizer * pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self.animator action:@selector(pinchGestureAction:)];
    
    [self.view addGestureRecognizer:pinch];
    self.transitioningDelegate = self;
}

 
- (void) configureView
{
    NSLog(@"%s",__func__);
    if (self.detailItem) {
        
    }
}

#pragma mark - <UIViewControllerTransitioningDelegate>
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return self;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForPresentation:(id <UIViewControllerAnimatedTransitioning>)animator
{
    return self.animator;
}

- (id <UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id <UIViewControllerAnimatedTransitioning>)animator
{
    return self.animator;
}

#pragma mark - <UIViewControllerAnimatedTransitioning>
-  (NSTimeInterval) transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 1.0f;
}

- (void) animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    [self startInteractiveTransition:transitionContext];
}

#pragma mark - <UIViewControllerInteractiveTransitioning>
- (void) startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController * src = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    CGPoint centerPoint = self.view.center;
    [UIView animateWithDuration:1.0 animations:^{
        src.view.frame = CGRectMake(centerPoint.x, centerPoint.y, 10, 10);
    } completion:^(BOOL finished){
        [transitionContext completeTransition:YES];
    }];
}
//- (CGFloat)completionSpeed
//{
//    return 10;
//}
- (UIViewAnimationCurve)completionCurve
{
    return UIViewAnimationCurveEaseInOut;
}

@end
